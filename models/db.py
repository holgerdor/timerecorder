# -*- coding: utf-8 -*-

# Make sure changes in modules are recognized
from gluon.custom_import import track_changes
track_changes(True)

db = DAL(*DAL_ARGS, **DAL_KEYWORDS)

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
#from gluon.contrib.login_methods.rpx_account import use_janrain
#use_janrain(auth, filename='private/janrain.key')

db.define_table('activity',
   Field('name',
         requires = IS_NOT_EMPTY()),
   Field('user_id', 'reference auth_user', 
         default=auth.user_id,
         readable=False,
         writable=False),
   format='%(name)s')

db.define_table('timerecord',
   Field('user_id', 'reference auth_user', default=auth.user_id),
   Field('activity_id', 'reference activity'),
   Field('timemark', 'datetime', default=request.now))

if auth.user_id:
    # Care about stop action
    ac = db(db.activity.user_id==auth.user_id).select().first()
    stop_activity_id = ac.id if ac else db.activity.insert(name="Stop") 

    # Find current activity
    tr = db(db.timerecord.user_id==auth.user_id).select().last()
    current_activity_id = tr.activity_id if tr else stop_activity_id
    


    

