# -*- coding: utf-8 -*-

from gluon.html import *
from datetime import datetime

class Record():
    def __init__(self, activity, start, end=None):
        self.activity = activity
        self.start = start
        self.end = end


class TimeReport():
    def __init__(self, **keywords):
        self.record_list = []
        self.keywords = keywords
        
    def add(self, record):
        self.record_list.insert(0, record)
        pass
    
    def xml(self):
        last_date = None
        result = DIV()
        for r in self.record_list:
            if r.start.date() != last_date:
                result.append(H3(r.start.date().strftime("%A %Y-%m-%d"))) 
                last_date = r.start.date()
                table = TABLE(**self.keywords)
                table.append(TR(TH('Activity'), TH('Start'), TH('End'), TH('Duration')))
                result.append(table)
            table.append(TR(TD(r.activity),
                            TD(r.start.strftime('%X')),
                            TD(r.end.strftime('%X') if r.end else 'now'),
                            TD((r.end-r.start) if r.end else (datetime.now()-r.start))))
                
        
        return str(result)
        
        