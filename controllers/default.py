# -*- coding: utf-8 -*-

from datetime import timedelta
from TimeReport import Record, TimeReport


@auth.requires_login()
def index():
    """
    List all possible activities and let user start and stop
    """
    rows = db(db.activity.user_id==auth.user_id).select()
    if current_activity_id != stop_activity_id:
        time_on_cur_act = request.now - db(db.timerecord.user_id==auth.user_id).select().last().timemark
    else:
        time_on_cur_act = 0
    return dict(activities=rows, 
                current_activity=db.activity[current_activity_id].name,
                stop_activity_id=stop_activity_id,
                current_activity_id=current_activity_id,
                time_on_cur_act=time_on_cur_act)

@auth.requires_login()
def record_time():    
    activity_id = request.args(0,cast=int) or redirect(URL('index'))
    if activity_id != current_activity_id:
        db.timerecord.insert(activity_id=activity_id)
    redirect(URL('index')) 

@auth.requires_login()
def summary():
    """
    Display the summary and the list of all record
    """
    
    response.subtitle = 'Sum of time spent for each activity'
    
    # First get activity names
    activity_names = dict()
    for act in db(db.activity.user_id==auth.user_id).select():
        activity_names[act.id] = act.name
    
    # Then process all the records 
    time_cons = dict()
    prev_act = stop_activity_id
    for row in db(db.timerecord.user_id==auth.user_id).select():
        if prev_act != stop_activity_id:
            time_cons[prev_act] = (time_cons[prev_act] if prev_act in time_cons else timedelta()) + (row.timemark - prev_timemark)            
        prev_act = row.activity_id
        prev_timemark = row.timemark
    if prev_act != stop_activity_id:
        # action is currently active
        time_cons[prev_act] = (time_cons[prev_act] if prev_act in time_cons else timedelta()) + (request.now - prev_timemark)
    
    time_cons_names = dict()
    for k, v in time_cons.iteritems():
        time_cons_names[activity_names[k]] = v
            
    return dict(time_cons_names=time_cons_names)            

@auth.requires_login()
def details():
    """
    Display the details of time spent grouped by day
    """
    
    time_report = TimeReport(_class="table table-striped table-bordered")
    
    # First get activity names
    activity_names = dict()
    for act in db(db.activity.user_id==auth.user_id).select():
        activity_names[act.id] = act.name
    
    # Then process all the records 
    prev_act = stop_activity_id
    for row in db(db.timerecord.user_id==auth.user_id).select():
        if prev_act != stop_activity_id:
            time_report.add(Record(activity_names[prev_act], prev_timemark, row.timemark))
        prev_act = row.activity_id
        prev_timemark = row.timemark
    if prev_act != stop_activity_id:
        # action is currently active
        time_report.add(Record(activity_names[prev_act], prev_timemark))
                
    return dict(time_report=time_report)            

@auth.requires_login()
def activity():
    """
    Manage your activities
    """
    response.subtitle = 'Manage your activities'
    grid = SQLFORM.grid(db.activity.user_id==auth.user_id, deletable=False)
    return dict(grid=grid)    


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())
